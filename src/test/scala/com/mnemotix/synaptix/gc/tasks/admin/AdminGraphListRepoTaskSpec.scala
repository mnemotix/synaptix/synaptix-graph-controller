/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.gc.tasks.admin

import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.graphdb.models.GraphDBRepository
import play.api.libs.json.{JsNull, Json}

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-11
  */

class AdminGraphListRepoTaskSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  "AdminGraphListRepoTask" should {
    val task = new AdminGraphListRepoTask("admin.graph.repo.list", AmqpClientConfiguration.exchangeName)
    "list active repositories" in {
      val message = AmqpMessage(Map.empty, JsNull)
      val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
      val msg = Json.parse(resultMessage.bytes.utf8String).as[AmqpMessage]
      msg.body.validate[Seq[GraphDBRepository]].isSuccess shouldBe true
    }
  }
}

