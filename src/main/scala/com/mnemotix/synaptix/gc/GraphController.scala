/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.gc

import java.net.ConnectException

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.amqp.api.rpc.AmqpRpcController
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpConnectionHandler}
import com.mnemotix.synaptix.gc.tasks.admin.{AdminGraphDropRepoTask, AdminGraphListRepoTask}
import com.mnemotix.synaptix.gc.tasks.create.{GraphCreateFromJsonLdTask, GraphCreateFromQueryTask}
import com.mnemotix.synaptix.gc.tasks.delete.GraphDeleteTask
import com.mnemotix.synaptix.gc.tasks.read.{GraphAskTask, GraphConstructTask, GraphDescribeTask, GraphSelectTask}
import com.mnemotix.synaptix.gc.tasks.update.GraphUpdateTask
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFClientConfiguration, RDFClientConnectException, RDFClientUnknownRepositoryException}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-12
  */

object GraphController extends App with LazyLogging {

  implicit val system = ActorSystem("GraphControllerSystem")
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContext = system.dispatcher

//  def initRdfClient(): Boolean = {
//    try {
//      logger.info("Initializing connection to RDF store...")
//      RDFClient.init()
//      logger.info("RDF client initilization [OK].")
//      true
//    } catch {
//      case ex: RDFClientUnknownRepositoryException =>
//        logger.error("RDF client initilization [KO]: " + ex.getMessage)
//        false
//      case ex: RDFClientConnectException =>
//        logger.error("RDF client initilization [KO]: " + ex.getMessage)
//        false
//      case t: Throwable =>
//        logger.error("RDF client initilization [KO]: " + t.getMessage)
//        false
//    }
//  }

  val controller = new AmqpRpcController("Graph Controller")

  logger.info("Graph controller starting...")

  RDFClient.mute()

  RDFClient.init()
//  while (!initRdfClient()) {
//    logger.error("Unable to initialize the connection to the triple store. Retrying in 2 seconds...")
//    Thread.sleep(2000)
//  }

  RDFClient.verbose()

  controller.registerTask("admin.graph.repo.drop", new AdminGraphDropRepoTask("admin.graph.repo.drop", AmqpClientConfiguration.exchangeName))
  controller.registerTask("admin.graph.repo.list", new AdminGraphListRepoTask("admin.graph.repo.list", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.create.triples", new GraphCreateFromJsonLdTask("graph.create.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.insert.triples", new GraphCreateFromQueryTask("graph.insert.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.select", new GraphSelectTask("graph.select", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.construct", new GraphConstructTask("graph.construct", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.ask", new GraphAskTask("graph.ask", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.describe", new GraphDescribeTask("graph.describe", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.update.triples", new GraphUpdateTask("graph.update.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.delete.triples", new GraphDeleteTask("graph.delete.triples", AmqpClientConfiguration.exchangeName))

  val starting = controller.start(true)
  logger.info(s"Graph controller successfully connected to repository manager [${RDFClientConfiguration.rootUri}] with credentials [${RDFClientConfiguration.user.get}, ${RDFClientConfiguration.pwd.get.map(_=>"*").mkString("")}]")

  starting onComplete {
    case Success(_) => logger.info("Graph controller started successfully.")
    case Failure(err) => {
      err.printStackTrace()
      sys.exit(1)
    }
  }

  sys addShutdownHook {
    Await.result(controller.shutdown, Duration.Inf)
  }
}
