/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.gc.tasks.admin

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import com.mnemotix.synaptix.graphdb.GraphDbRestApiClient
import play.api.libs.json.{JsArray, Json}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-03-12
 */

class AdminGraphListRepoTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = Future {
    logger.debug(s"Message received : \n${Json.prettyPrint(Json.parse(msg.bytes.utf8String))}")
    //    logger.debug(s"ReplyTo : ${msg.properties.getReplyTo}/${msg.properties.getCorrelationId}")
    Json.parse(msg.bytes.utf8String).validate[AmqpMessage].isSuccess match {
      case true => {
        val result = Json.toJson(GraphDbRestApiClient.listRepositories())
        getResponseMessage(result, "JSON", "OK", msg.properties)
      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
      }
    }
  }

}