import sbt._

object Version {
  lazy val scalaVersion = "2.12.10"
  lazy val scalaTest = "3.1.1"
  lazy val synaptixVersion = "0.1.11-SNAPSHOT"
  lazy val akkaVersion = "2.6.4"
  lazy val logback = "1.2.3"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akkaVersion
  lazy val amqpLib = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val rdfToolkit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
}
